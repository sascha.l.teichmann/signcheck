# signcheck

A simple tool to check the signature of a file,
given am ascii armored public key, an ascii armored signature and a file.

## Build

```
go build
```

## Usage

```
./signcheck pub.asc sig.asc data
```
