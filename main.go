package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/ProtonMail/gopenpgp/v2/crypto"
)

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func main() {
	flag.Parse()
	if flag.NArg() < 3 {
		log.Fatalln("missing argument")
	}
	var (
		pubKeyFile = flag.Arg(0)
		sigFile    = flag.Arg(1)
		docFile    = flag.Arg(2)
	)

	// Load public key
	keyData, err := os.ReadFile(pubKeyFile)
	check(err)

	pubKey, err := crypto.NewKeyFromArmored(string(keyData))
	check(err)

	keyRing, err := crypto.NewKeyRing(pubKey)
	check(err)

	// Load signature
	sigData, err := os.ReadFile(sigFile)
	check(err)

	sig, err := crypto.NewPGPSignatureFromArmored(string(sigData))
	check(err)

	// Load document
	docData, err := os.ReadFile(docFile)
	check(err)

	message := crypto.NewPlainMessage(docData)
	t := crypto.GetUnixTime()

	if err := keyRing.VerifyDetached(message, sig, t); err != nil {
		fmt.Printf("validation failed: %v\n", err)
	} else {
		fmt.Println("validation succeeded")
	}
}
